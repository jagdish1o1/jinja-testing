import json
import urllib.parse
import re
import datetime
import os
from jinja2 import Environment, FileSystemLoader
import htmlmin

letter = ["letter-a.json", "letter-b.json", "letter-c.json", "letter-char.json", "letter-d.json", "letter-e.json",
           "letter-f.json", "letter-g.json", "letter-h.json", "letter-i.json", "letter-j.json", "letter-k.json",
           "letter-l.json", "letter-m.json", "letter-n.json", "letter-o.json", "letter-p.json", "letter-q.json",
           "letter-r.json", "letter-s.json", "letter-t.json", "letter-u.json", "letter-v.json", "letter-w.json",
           "letter-x.json", "letter-y.json", "letter-z.json"]
for dataset in letter:
    with open(dataset, "r") as d:
        data = json.load(d)

    # Generate Static App Pages
    for app in data:
        cwd = os.getcwd()
        templatefolder = cwd + "/templates"
        fileloader = FileSystemLoader("templates")
        env = Environment(loader=fileloader)
        date = datetime.date.today()
        today = date.strftime('%d, %b %Y')
        genhtml = env.get_template("app.html").render(game=app, today=today)
        namestr = 'id' + app['id']
        filename = urllib.parse.quote(namestr + ".html")
        path = cwd + "/www/app/" + filename

        if not os.path.exists("www"):
            os.makedirs('www')
        if not os.path.exists("www/app"):
            os.makedirs('www/app')

        with open(path, "w") as f:
            f.write(genhtml)
            print(path)

    # Generate index.html page
    hcwd = os.getcwd()
    templatefolderforhome = hcwd + "/templates"
    fileloader = FileSystemLoader(templatefolderforhome)
    henv = Environment(loader=fileloader)
    genhome = henv.get_template("home.html").render(games=data)
    minifiedgenhome = htmlmin.minify(genhome)
    hfilename = "index.html"
    hpath = hcwd + "/www/" + hfilename
    with open(hpath, "w") as f:
        f.write(minifiedgenhome)